# nautilus-script-remove-meta-data

This nautilus script removes meta data from img files.

It uses `libimage-exiftool-perl`. Install it first with:

`sudo apt install libimage-exiftool-perl`

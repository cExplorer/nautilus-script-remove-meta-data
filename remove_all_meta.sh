#!/bin/bash

# Dieses Skript entfernt Meta Daten in Bilddateien.
#
# This script removes meta data from img files.
#
# You need to be running Nautilus 1.0.3+ to use scripts.
# To install as Nautilus script consider copying to filename with spaces.
# This is for better reading the Nautilus content menu
# For all users, type for example:
# sudo cp my_script "/usr/share/nautilus-scripts/my script"
# sudo chmod +x "/usr/share/nautilus-scripts/my script"
# sudo ln -s "/usr/share/nautilus-scripts/my_script" "~/.local/share/nautilus/scripts/my script"
#
# For local user, type for example:
# cp my_script ".local/share/nautilus/scripts/my script"
# chmod u+x ".local/share/nautilus/scripts/my script"
#
# dependent on: libimage-exiftool-perl
#
# Author: Joerg Sorge
# Distributed under the terms of GNU GPL version 2 or later
# Copyright (C) Joerg Sorge, mail joergsorge de
# 2021-12-03


function f_choose_msg_lang () {
	local_locale=$(locale | grep LANGUAGE | cut -d= -f2 | cut -d_ -f1)
	if [ $local_locale == "de" ]; then
		msg[1]="installiert"
		msg[2]="Meta-Daten entfernen..."
		msg[3]="Entferne Meta Daten: Bearbeite Dateien..."
		msg[4]="Abgebrochen..."

		err[1]=" ist nicht installiert, Bearbeitung nicht moeglich."
		err[2]="Fehler beim Entfernen"
	else
		msg[1]="installed"
		msg[2]="Remove meta data..."
		msg[3]="Remove meta data: work on files..."
		msg[4]="Canceled..."

		err[1]=" not installed, work not possible."
		err[2]="Error by removing meta data"
	fi
}


function f_check_package () {
	package_install=$1
	if dpkg-query -s $1 2>/dev/null|grep -q installed; then
		echo "$package_install ${msg[1]}"
	else
		zenity --error --text="$package_install ${err[1]}"
		exit
	fi
}

# switch lang
f_choose_msg_lang
# check for packages
f_check_package "libimage-exiftool-perl"

# this works not properly if multible files are selected
#echo -n "${NAUTILUS_SCRIPT_SELECTED_FILE_PATHS}" | while read file ; do
# so we use this loop

for file in "$@"; do
report "remove all meta data"
(
	filename=$(basename "$file")
	# echo and progress will pulsate
	echo "10"
	echo "# ${msg[2]}\n$filename"
	message=$(exiftool -all= "$file" 2>&1)
	error=${message:0:5}
	if [ "$error" == "Error" ]
		then
		echo "$message" | zenity --title="${err[2]} " --text-info --width=500 --height=200
	fi

) | zenity --progress \
           --title="${msg[3]}" --text="..." --width=500 --pulsate --auto-close

if [ "$?" = -1 ] ; then
	zenity --error --text="${msg[4]}"
fi
done
exit
